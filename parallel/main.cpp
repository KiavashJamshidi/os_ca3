#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <limits>
#include <iomanip>
#include <cstdlib>
#include <pthread.h>

using namespace std;

class Mobile {
    public:
        vector<float> details;
        float price_range;
        float min,max;
        Mobile() {}
        void addDetail(float detail){
            details.push_back(detail);
        }
        void setPrice() {
            price_range = details[details.size()-1];
            details.erase(details.begin() + details.size()-1);
        }

};

class Weight{
    public:
        vector<float> details;
        float bias;
        Weight() {}
        void addDetail(float detail){
            details.push_back(detail);
        }
        void setBias() {
            bias = details[details.size()-1];
            details.erase(details.begin() + details.size()-1);
        }
};

void getWeightCsv(vector<Weight> &weights,char* argv[]){
    fstream file;
    string dir = argv[1];
    file.open(dir + string("weights.csv"));
    string line;
    vector<string> lines;
    Weight w;

    while(getline(file,line)) {
        lines.push_back(line);
    }

    file.close();

    for (int each=1;each<lines.size();each++) {
        Weight w;
        vector<string> v;
        stringstream ss(lines[each]);
        while (ss.good()) {
            string substr;
            getline(ss, substr, ',');
            v.push_back(substr);
        }
        for (size_t i = 0; i < v.size(); i++)
            w.addDetail(stof(v[i]));
        w.setBias();
        weights.push_back(w);
    }
}

void getTrainCsv(vector<Mobile> &mobiles,char* argv[]){
    fstream file;
    string dir = argv[1];
    file.open(dir + string("train.csv"));
    string line;
    vector<string> lines;
    Weight w;

    while(getline(file,line)) {
        lines.push_back(line);
    }

    file.close();

    for (int each=1;each<lines.size();each++) {
        Mobile m;
        vector<string> v;
        stringstream ss(lines[each]);
        while (ss.good()) {
            string substr;
            getline(ss, substr, ',');
            v.push_back(substr);
        }
        for (size_t i = 0; i < v.size(); i++)
            m.addDetail(stof(v[i]));
        m.setPrice();
        mobiles.push_back(m);
    }
}

void normilize(vector<Mobile> &mobiles){
    for (int eachCol=0;eachCol < mobiles[0].details.size();eachCol++){
        float min = mobiles[0].details[eachCol];
        float max = mobiles[0].details[eachCol];
        for (int i=1;i<mobiles.size();i++){
            if (mobiles[i].details[eachCol] < min)
                min = mobiles[i].details[eachCol];
            if (mobiles[i].details[eachCol] > max)
                max = mobiles[i].details[eachCol];
        }

        for (int i=0;i<mobiles.size();i++){
            float newX = (mobiles[i].details[eachCol] - min) / (max - min);
            mobiles[i].details[eachCol] = newX;
        }
    }
}

struct thread_data {
    vector<Weight> weights;
    vector<Mobile> mobiles;
    long i;
    float accuracy;
};

void* calculateThread(void* threadArgs){
    struct thread_data *myData;
    myData = (struct thread_data *) threadArgs;
    float max = numeric_limits<float>::min();
    for (int w=0;w<myData->weights.size();w++){
        float result = 0;
        for (int det=0;det<myData->weights[w].details.size();det++){
            result += myData->mobiles[myData->i].details[det] * myData->weights[w].details[det];
        }
        result += myData->weights[w].bias;
        if (result > max) max = result;
    }
    int finalPrice = round(max);
    if (finalPrice == myData->mobiles[myData->i].price_range) (myData->accuracy)++;
    pthread_exit(NULL);
}

void calculateAccuracy(vector<Weight> weights,vector<Mobile> mobiles){
    float accuracy = 0;

    pthread_t threads[mobiles.size()];

    struct thread_data threadData;
    threadData.mobiles = mobiles;
    threadData.weights = weights;
    threadData.accuracy = 0;
    for (long i=0;i<mobiles.size();i++) {
        threadData.i = i;
        int failed = pthread_create(&threads[i],NULL,calculateThread,(void *) &threadData);
        accuracy = threadData.accuracy;
    }
    accuracy /= mobiles.size();
    cout << "‫‪Accuracy:‬‬ " <<setprecision(2) << fixed << accuracy * 100 <<"%"<< endl;
    // pthread_exit(NULL);
}


int main(int argc,char* argv[]){
    vector<Weight> weights;
    vector<Mobile> mobiles;
    getWeightCsv(weights,argv);
    getTrainCsv(mobiles,argv);

    normilize(mobiles);

    calculateAccuracy(weights,mobiles);
}